﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace BookManual.Controller
{
    class Register
    {
        
        FileInfo fileBookInfo = new FileInfo("bookInfomation.txt");                         // 도서 데이터베이스
        FileInfo fileMemberInfo = new FileInfo("memberInformation.txt");                    // 회원 데이터베이스
        FileInfo filereportInfo = new FileInfo("reportInformation.txt");                    // 반납 및 대출 기록 데이터 베이스
        public List<Model.BookClass> bookList = new List<Model.BookClass>();                // 도서 관련 리스트 생성
        public List<Model.MemberClass> memberList = new List<Model.MemberClass>();          // 회원 리스트 생성
        public List<string> reportList = new List<string>();                                // 반납 및 대출 기록 리스트 생성
        Model.BookClass bookInfo = new Model.BookClass();                                   // 도서 관련 객체 생성
        Model.MemberClass memberInfo = new Model.MemberClass();                             // 회원 관련 객체 생성
        

        public List<Model.BookClass> BookRead()
        {
            if (!fileBookInfo.Exists)       //파일이 없을경우

            {
                Stream bookText = new FileStream("bookInfomation.txt", FileMode.Create);        //파일 생성
                bookText.Close();
            }

            else

            {

                if (fileBookInfo.Length != 0)   //기존의 데이타를 가지고 있다면.

                {
                    Stream bookOpen = new FileStream("bookInfomation.txt", FileMode.Open); //일단 불러온다.
                    BinaryFormatter deserializer = new BinaryFormatter();
                    bookList = (List<Model.BookClass>)deserializer.Deserialize(bookOpen);       //역직렬화,리스트에 저장함.
                    bookOpen.Close();
                }

            }

            return bookList;
        }

        public void BookWrite(List<Model.BookClass> bookClass)

        {
            bookList = bookClass;   // 리스트 데이터를 받아온다
            Stream ws = new FileStream("bookInfomation.txt", FileMode.Create);      //텍스트 파일 생성
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(ws, bookList);     //직렬화(저장)
            ws.Close();
        }

        public List<Model.MemberClass> MemberRead()
        {
            if (!fileMemberInfo.Exists)       //파일이 없을경우

            {
                Stream memberText = new FileStream("memberInformation.txt", FileMode.Create);    //텍스트 파일 생성
                memberText.Close();
            }

            else

            {

                if (fileMemberInfo.Length != 0)   //기존의 데이타를 가지고 있다면.

                {
                    Stream memberOpen = new FileStream("memberInformation.txt", FileMode.Open); //일단 불러온다.
                    BinaryFormatter deserializer = new BinaryFormatter();
                    memberList = (List<Model.MemberClass>)deserializer.Deserialize(memberOpen);       //역직렬화,리스트에 저장함.
                    memberOpen.Close();
                }

            }

            return memberList;
        }

        public void MemberWrite(List<Model.MemberClass> memberClass)

        {
            memberList = memberClass;   //  매개변수를 통해 리스트를 받는다
            Stream ws = new FileStream("memberInformation.txt", FileMode.Create);    //텍스트 파일을 생성한다
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(ws, memberList);     //직렬화(저장)
            ws.Close();
        }

        public List<string> ReportRead()
        {
            if (!filereportInfo.Exists)       //파일이 없을경우

            {
                Stream reportText = new FileStream("reportInformation.txt", FileMode.Create);    //텍스트 파일 생성
                reportText.Close();
            }

            else

            {

                if (filereportInfo.Length != 0)   //기존의 데이타를 가지고 있다면.

                {
                    Stream reportOpen = new FileStream("reportInformation.txt", FileMode.Open); //일단 불러온다.
                    BinaryFormatter deserializer = new BinaryFormatter();
                    reportList = (List<string>)deserializer.Deserialize(reportOpen);       //역직렬화,리스트에 저장함.
                    reportOpen.Close();
                }

            }

            return reportList;
            
        }

        public void ReportWrite(List<string> example)
        {
            reportList = example;   //  매개변수를 통해 리스트를 받는다
            Stream ws = new FileStream("reportInformation.txt", FileMode.Create);    //텍스트 파일을 생성한다
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(ws, reportList);     //직렬화(저장)
            ws.Close();
        }
    }
}
