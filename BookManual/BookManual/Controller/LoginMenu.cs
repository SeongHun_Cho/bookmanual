﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManual.Controller
{
    class LoginMenu
    {
        View.MenuView view = new View.MenuView();
        Controller.InfoInput info = new Controller.InfoInput();
        Controller.Register regist = new Controller.Register();
        List<Model.BookClass> bookList = new List<Model.BookClass>(); //책 관리 리스트 생성
        List<Model.MemberClass> memberList = new List<Model.MemberClass>();  // 멤버 관리 리스트 생성
        List<string> reportList = new List<string>();
        
        string outcheck;
        string identy;
        string check;
        bool flag;
        int y = 18;
        ConsoleKeyInfo direction;

        public string MemberCheck()
        {
            outcheck = "";
            flag = true;
            bool wrong1 = true;
            bool wrong2 = true;
            while (flag)
            {
                while (wrong1)
                {
                    int IDcheck = 0;
                    
                    view.Title();
                    memberList = regist.MemberRead();

                    check = info.ClassInput("회원 ID를 입력해주세요 : ", 1);


                    if (check == "out") { outcheck = "out"; break; }
                    else
                    {
                        for (int checknum = 0; checknum < memberList.Count(); checknum++)
                        {
                            if (check == memberList[checknum].MemberId)
                            {
                                IDcheck = IDcheck + 1;
                                break;
                            }
                        }
                        if (IDcheck != 0) {  wrong2 = true; break; }
                        else { Console.Clear(); wrong2 = false;wrong1 = true;ClearPartOfConsole(Console.CursorTop); }
                    }
                }
                if (outcheck == "out") { break; }
                while (wrong2)
                {
                    int PassWordcheck = 0;
                    string check2;
                    memberList = regist.MemberRead();

                    check2 = info.ClassInput("회원 PassWord를 입력해주세요 : ", 1);



                    if (check2 == "out") { break; }
                    else
                    {
                        for (int checknum = 0; checknum < memberList.Count(); checknum++)
                        {
                            if (check2 == memberList[checknum].MemberPassWord)
                            {
                                PassWordcheck = PassWordcheck + 1;
                                break;
                            }

                        }
                        if (PassWordcheck != 0) { flag = false; outcheck = "out"; break; }
                        else { int line = Console.CursorTop;  ClearPartOfConsole(line-1);  wrong1 = false;wrong2 = true; }

                    }
                }
                if (outcheck == "out") { break; }
            }
            return check;
        }

        public void MainMenu(string ID)
        {
            identy = ID;
            while (true)
            {
                view.Title();
                view.LoginMenu();
                flag = true;
               
                Console.SetCursorPosition(30, 15);
                while (flag)
                {
                    direction = Console.ReadKey();
                    if (direction.Key == ConsoleKey.Enter)
                    {
                        switch (y)
                        {
                            case 15:
                                Console.Clear();
                                BookSearchMenu(identy);
                                flag = false;
                                break;
                            case 18:
                                Console.Clear();
                                view.Title();
                                BookBorrow(identy);
                                GoBack(identy);
                                flag = false;
                                break;
                            case 21:
                                Console.Clear();
                                view.Title();
                                BookReturn(identy);
                                GoBack(identy);
                                flag = false;
                                break;
                            case 24:
                                Console.Clear();
                                view.Title();
                                BookListPrint();
                                GoBack(identy);
                                Console.Clear();
                                flag = false;
                                break;
                            case 27:
                                Console.Clear();
                                view.Title();
                                MemberInfoPrint(identy);
                                GoBack(identy);
                                Console.Clear();
                                flag = false;
                                break;
                            case 30:
                                Console.Clear();
                                Controller.MainSystem system = new Controller.MainSystem();
                                system.StartMenu();
                                break;
                            case 33:
                                int temporary = Convert.ToInt32(Console.CursorTop);
                                Console.SetCursorPosition(30, temporary + 2);
                                Environment.Exit(0);
                                break;
                          

                        }
                    }
                    


                    switch (direction.Key)
                    {
                        case ConsoleKey.UpArrow:
                            y = y - 3;
                            if (y < 15)
                            {
                                y = 33;
                            }
                            Console.SetCursorPosition(30, y);
                            break;
                        case ConsoleKey.DownArrow:
                            y = y + 3;
                            if (y > 33)
                            {
                                y = 15;
                            }
                            Console.SetCursorPosition(30, y);
                            break;
                        case ConsoleKey.Escape:
                            outcheck = "out";
                            flag = false;
                            break;
                    }
                }
                if (outcheck == "out") { Console.Clear(); break; }
            }
        }
        

        public void BookSearchMenu(string ID)
        {
            while (true)
            {
                view.Title();
                view.BookSearchView();
                flag = true;

                Console.SetCursorPosition(30, 18);
                while (flag)
                {
                    direction = Console.ReadKey();
                    if (direction.Key == ConsoleKey.Enter)
                    {
                        switch (y)
                        {
                            case 18:
                                Console.Clear();
                                view.Title();
                                BookSearch(1,ID);
                                GoBack(ID);
                                flag = false;
                                break;
                            case 21:
                                Console.Clear();
                                view.Title();
                                BookSearch(2,ID);
                                GoBack(ID);
                                flag = false;
                                break;
                            case 24:
                                Console.Clear();
                                view.Title();
                                BookSearch(3,ID);
                                GoBack(ID);
                                flag = false;
                                break;
                            case 27:
                                Console.Clear();
                                MainMenu(identy);
                                flag = false;
                                break;
                           


                        }
                    }
                  

                    switch (direction.Key)
                    {
                        case ConsoleKey.UpArrow:
                            y = y - 3;
                            if (y < 18)
                            {
                                y = 27;
                            }
                            Console.SetCursorPosition(30, y);
                            break;
                        case ConsoleKey.DownArrow:
                            y = y + 3;
                            if (y > 27)
                            {
                                y = 18;
                            }
                            Console.SetCursorPosition(30, y);
                            break;

                    }
                }
                
            }
        }

        public void BookSearch(int search,string ID)
        {
            bookList = regist.BookRead();
            switch (search) {
                case 1:
                Console.Write("책 이름을 입력해주세요 : ");
                    break;
                case 2:
                    Console.Write("책 저자를 입력해주세요 : ");
                    break;
                case 3:
                    Console.Write("출판사를 입력해주세요 : ");
                    break;
        }
            string condition = info.KeyEsc(2);
            if (condition == "out")
            {
                GoBack(ID);
            }
            else
            {
                if (search == 1) { 
                for (int repeat = 0; repeat < bookList.Count(); repeat++)
                {
                    if (bookList[repeat].BookName.Contains(condition))
                    {
                        BookPrint(repeat);

                    }
                }
            }
                else if (search == 2)
                {
                    for (int repeat = 0; repeat < bookList.Count(); repeat++)
                    {
                        if (bookList[repeat].BookAuthor.Contains(condition))
                        {
                            BookPrint(repeat);

                        }
                    }
                }

                else if (search == 3)
                {

                    for (int repeat = 0; repeat < bookList.Count(); repeat++)
                    {
                        if (bookList[repeat].Publisher.Contains(condition))
                        {
                            BookPrint(repeat);
                        }
                    }
                }
            }
        }

        public void BookBorrow(string borr)
        {
            string ID = borr;
            bookList = regist.BookRead();
            memberList = regist.MemberRead();
            reportList = regist.ReportRead();
            BookSearch(1,ID);
            Console.Write("대출할 책의 도서번호를 입력해주세요  :  ");
            
            string nocheck = info.KeyEsc(2);
            for(int repeatcheck = 0; repeatcheck < bookList.Count(); repeatcheck++)
            {
                if (bookList[repeatcheck].BookId == nocheck)
                {
                    bookList[repeatcheck].Amount = Convert.ToString(Convert.ToInt32(bookList[repeatcheck].Amount) - 1);
                    for(int repeat = 0; repeat < memberList.Count(); repeat++)
                    {
                        if (memberList[repeat].MemberId == ID)
                        {
                            reportList.Add(DateTime.Now.ToString() + " 에 " + "'" + memberList[repeat].MemberName + "' 가  " + bookList[repeatcheck].BookName + "  을 대출했습니다.");
                            List<string> bookBorrow = new List<string>();
                            if (memberList[repeat].bookBorrow.Count() != 0)
                            {
                                bookBorrow = memberList[repeat].bookBorrow;
                            }
                            Console.Write("\n\n       대출  성공!!!!!!    \n\n");
                            memberList[repeat].MemberBorrow = memberList[repeat].MemberBorrow + 1;
                            bookBorrow.Add(bookList[repeatcheck].BookName);
                            memberList[repeat].bookBorrow = bookBorrow;
                            break;
                        }
                    }
                    break;
                }
            }
            regist.ReportWrite(reportList);
            regist.BookWrite(bookList);
            regist.MemberWrite(memberList);
            
        }
    

        public void BookReturn(string ID)
        {
            while (true)
            {
                identy = ID;
                bookList = regist.BookRead();
                memberList = regist.MemberRead();
                reportList = regist.ReportRead();

                for (int repeat = 0; repeat < memberList.Count(); repeat++)
                {
                    if (memberList[repeat].MemberId == identy)
                    {
                        for (int repeat2 = 0; repeat2 < memberList[repeat].bookBorrow.Count(); repeat2++)
                        {
                            Console.Write("\n\n"+(repeat2 + 1) + ". ");
                            Console.WriteLine(memberList[repeat].bookBorrow[repeat2]+"\n\n");
                        }

                        break;
                    }
                }

                Console.Write("반납할 도서 번호를 입력해주세요  :   ");
                string num = info.KeyEsc(2);
                if (num == "out") { GoBack(identy); }

                for (int repeat = 0; repeat < memberList.Count(); repeat++)
                {
                    if (memberList[repeat].MemberId == identy)
                    {
                        for (int repeat2 = 0; repeat2 < bookList.Count(); repeat2++)
                        {
                            if (bookList[repeat2].BookName == memberList[repeat].bookBorrow[Convert.ToInt32(num) - 1]){
                                bookList[repeat2].Amount = Convert.ToString(Convert.ToInt32(bookList[repeat2].Amount) + 1);
                                Console.Write("\n\n       반납    성공!!!!!!     \n\n");
                                reportList.Add(DateTime.Now.ToString() + " 에 " + "'" + memberList[repeat].MemberName + "' 가  " + bookList[repeat2].BookName + "  을 반납했습니다.");
                                break;
                            }
                                
                        }
                        memberList[repeat].bookBorrow.RemoveAt(Convert.ToInt32(num) - 1);
                        memberList[repeat].MemberBorrow = memberList[repeat].MemberBorrow - 1;
                        
                        break;
                    }
                }
                regist.ReportWrite(reportList);
                regist.MemberWrite(memberList);
                regist.BookWrite(bookList);
                break;
            }
            
        }

        public void BookListPrint()
        {
            bookList = regist.BookRead();
            view.BookListView();
            for (int total = 0; total < bookList.Count(); total++)
            {

                Console.WriteLine("                도서번호 : " + bookList[total].BookId);
                Console.WriteLine("                책 이 름 : " + bookList[total].BookName);
                Console.WriteLine("                작    가 : " + bookList[total].BookAuthor);
                Console.WriteLine("                출 판 사 : " + bookList[total].Publisher);
                Console.WriteLine("                가    격 : " + bookList[total].Price);
                Console.WriteLine("                수    량 : " + bookList[total].Amount + "\n\n");
                Console.WriteLine("            -----------------------------------------------\n\n");
            }
        }




        public void BookPrint(int num)
        {
            bookList = regist.BookRead();

                Console.WriteLine("            -----------------------------------------------\n\n");
                Console.WriteLine("                도서번호 : " + bookList[num].BookId);
                Console.WriteLine("                책 이 름 : " + bookList[num].BookName);
                Console.WriteLine("                작    가 : " + bookList[num].BookAuthor);
                Console.WriteLine("                출 판 사 : " + bookList[num].Publisher);
                Console.WriteLine("                가    격 : " + bookList[num].Price);
                Console.WriteLine("                수    량 : " + bookList[num].Amount + "\n\n");
                Console.WriteLine("            -----------------------------------------------\n\n");
            
        }

        public void MemberInfoPrint(string ID)
        {
            identy = ID;
            memberList = regist.MemberRead();

            for(int repeat = 0; repeat < memberList.Count(); repeat++)
            {
                if (memberList[repeat].MemberId == identy)
                {
                    Console.WriteLine("                회원 I D : " + memberList[repeat].MemberId);
                    Console.WriteLine("                회원이름 : " + memberList[repeat].MemberName);
                    Console.WriteLine("                회원나이 : " + memberList[repeat].MemberAge);
                    Console.WriteLine("                폰 번 호 : " + memberList[repeat].MemberPhoneNumber);
                    Console.WriteLine("                주    소 : " + memberList[repeat].MemberAddress);
                    Console.WriteLine("                대출권수 : " + memberList[repeat].MemberBorrow );
                    Console.Write("                대출한책 : ");
                    for(int repeat2 = 0; repeat2 < memberList[repeat].bookBorrow.Count(); repeat2++)
                    {
                        Console.Write("  '" + memberList[repeat].bookBorrow[repeat2] + "'  ");
                    }
                    Console.WriteLine("\n\n            -----------------------------------------------\n\n");
                }
            }


        }

        public void GoBack(string conty)
        {
            bool flag = true;
            int position = Console.CursorTop;
            Console.SetCursorPosition(0, position);
            view.BackMenu();
            position = position + 2;
            Console.SetCursorPosition(30, position);
            int standard = position;
            ConsoleKeyInfo direction;
            while (flag)
            {
                direction = Console.ReadKey();
                if (direction.Key == ConsoleKey.Enter)
                {
                    if (position == standard)
                    {
                        Console.Clear();
                        MainMenu(conty);
                    }
                    else if (position == standard + 3)
                    {
                        Console.Clear();
                        Controller.MainSystem mainSystem = new Controller.MainSystem();
                        mainSystem.StartMenu();
                    }


                }

                else
                {
                    switch (direction.Key)
                    {
                        case ConsoleKey.UpArrow:
                            position = position - 3;
                            if (position < standard)
                            {
                                position = standard + 3;
                            }
                            Console.SetCursorPosition(30, position);
                            break;
                        case ConsoleKey.DownArrow:
                            position = position + 3;
                            if (position > standard + 3)
                            {
                                position = standard;
                            }
                            Console.SetCursorPosition(30, position);
                            break;

                    }
                }
            }
        }


        public void EmptyLine() //한줄 비워주는 메소드
        {
            Console.Write("                                                                                                                                                            ");
        }

        public void ClearPartOfConsole(int line)
        {
            Console.SetCursorPosition(0, line);
            EmptyLine();
            Console.SetCursorPosition(0, line);
        }






    }
}
