﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManual.View
{
    class MenuView
    {
        public void Title()
        {
            Console.WriteLine("======================================================================================================\n\n");
            Console.WriteLine("    ■            ■  ■■■■■■    ■■■■■■            ■        ■■■■■■     ■        ■ ");
            Console.WriteLine("    ■            ■  ■        ■■  ■        ■■         ■■       ■        ■■     ■    ■   ");
            Console.WriteLine("    ■            ■  ■          ■  ■          ■        ■  ■      ■          ■       ■■     ");
            Console.WriteLine("    ■            ■  ■■■■■■■  ■■■■■■■       ■    ■     ■■■■■■■        ■      ");
            Console.WriteLine("    ■            ■  ■          ■  ■      ■■        ■■■■■    ■      ■■          ■      ");
            Console.WriteLine("    ■            ■  ■        ■■  ■        ■■     ■        ■   ■        ■■        ■      ");
            Console.WriteLine("    ■■■■■■  ■  ■■■■■■    ■          ■■  ■          ■  ■          ■■      ■      \n\n");
            Console.WriteLine("============================================================================뒤로가기 : ESC============\n\n");

        }

        public void StartMenu()
        {
            Console.WriteLine("                               ▶  로    그    인  ◀\n\n");
            Console.WriteLine("                               ▶  회  원  가  입  ◀\n\n");
            Console.WriteLine("                               ▶  관    리    자  ◀\n\n");
            Console.WriteLine("                               ▶  종          료  ◀");
            
            
        }

        public void LoginMenu()
        {
            Console.WriteLine("                               ▶  책    검    색  ◀\n\n");
            Console.WriteLine("                               ▶  책    대    출  ◀\n\n");
            Console.WriteLine("                               ▶  책    반    납  ◀\n\n");
            Console.WriteLine("                               ▶  책  리  스  트  ◀\n\n");
            Console.WriteLine("                               ▶  나의 회원 정보  ◀\n\n");
            Console.WriteLine("                               ▶  로  그  아  웃  ◀\n\n");
            Console.WriteLine("                               ▶  종          료  ◀");
        }

        public void ManagerMenu()
        {
            Console.WriteLine("--------------------------------관   리   자   모   드---------------------------------------\n\n");
            Console.WriteLine("                               ▶  회 원 리 스 트  ◀\n\n");
            Console.WriteLine("                               ▶  책  리  스  트  ◀\n\n");
            Console.WriteLine("                               ▶  회  원  검  색  ◀\n\n");
            Console.WriteLine("                               ▶  회  원  삭  제  ◀\n\n");
            Console.WriteLine("                               ▶  책 정 보 수 정  ◀\n\n");
            Console.WriteLine("                               ▶  신 규 책 등 록  ◀\n\n");
            Console.WriteLine("                               ▶  책    삭    제  ◀\n\n");
            Console.WriteLine("                               ▶  대여 반납 기록  ◀\n\n");
            Console.WriteLine("                               ▶  메뉴로돌아가기  ◀");
        }

        public void BackMenu()
        {
            Console.Write("\n\n");
            Console.WriteLine("                               ▶  전으로돌아가기  ◀\n\n");
            Console.WriteLine("                               ▶  로  그  아  웃  ◀\n\n");
        }

        public void BookListView()
        {
            Console.Write("\n\n                         책 목록                           ");
            Console.Write("\n\n            -----------------------------------------------\n\n");
        }

        public void MemberListVIew()
        {
            Console.Write("\n\n                       회원 목록                           ");
            Console.Write("\n\n            -----------------------------------------------\n\n");
        }

        public void BookSearchView()
        {
            Console.WriteLine("--------------------------------책       검        색---------------------------------------\n\n");
            Console.WriteLine("                               ▶  책 이 름 으 로  ◀\n\n");
            Console.WriteLine("                               ▶  저 자 명 으 로  ◀\n\n");
            Console.WriteLine("                               ▶  출  판  사  로  ◀\n\n");
            Console.WriteLine("                               ▶  메뉴로돌아가기  ◀");
        }

    }
}
