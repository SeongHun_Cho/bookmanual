﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManual.Model
{
    [Serializable]
    public class BookClass
    {
        private string bookId; 
        private string bookName;
        private string bookAuthor;
        private string publisher;
        private string price;
        private string amount;

        public BookClass() { }

        public string BookId
        {
            get { return bookId; }
            set { bookId = value; }
        }

        public string BookName
        {
            get { return bookName; }
            set { bookName = value; }
        }

        public string BookAuthor
        {
            get { return bookAuthor; }
            set { bookAuthor = value; }
        }

        public string Publisher
        {
            get { return publisher; }
            set { publisher = value; }
        }

        public string Price
        {
            get { return price; }
            set { price = value; }
        }

        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }
    }
}
