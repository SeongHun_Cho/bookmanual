﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace BookManual.Controller
{
    class InfoInput
    {

        public List<Model.BookClass> bookList = new List<Model.BookClass>();    //도서 관리  리스트 생성
        public List<Model.MemberClass> memberList = new List<Model.MemberClass>();  // 멤버 관리 리스트 생성
        public List<string> bookBorrow = new List<string>();
        View.MenuView view = new View.MenuView();
        Controller.Register regist = new Controller.Register();
        // ConsoleKeyInfo back;
        // string temporary;
        public void BookInput()
        {
            Model.BookClass bookInfo = new Model.BookClass();
            while (true)
            {
                bookList = regist.BookRead();

                bookInfo.BookId = ClassInput("도서번호를 입력하세요 :  ", 1);
                if (bookInfo.BookId == "out") { break; }

                bookInfo.BookName = ClassInput("도서제목을 입력하세요 :  ", 2);
                if (bookInfo.BookName == "out") { break; }

                bookInfo.BookAuthor = ClassInput("작가이름을 입력하세요 :  ", 2);
                if (bookInfo.BookAuthor == "out") { break; }

                bookInfo.Publisher = ClassInput("출판사를 입력하세요 :  ", 2);
                if (bookInfo.Publisher == "out") { break; }

                bookInfo.Price = ClassInput("책 가격을 입력하세요 :  ", 2);
                if (bookInfo.Price == "out") { break; }

                bookInfo.Amount = ClassInput("책 수량을 입력하세요 :  ", 2);
                if (bookInfo.Amount == "out") { break; }


                bookList.Add(bookInfo);
                regist.BookWrite(bookList);
                Console.Clear();
                break;
            }


        }

        public void MemberInput()
        {
            Model.MemberClass memberInfo = new Model.MemberClass();
            while (true)
            {
                memberList = regist.MemberRead();

                memberInfo.MemberId = ClassInput("회원 ID를 입력하세요 :  ", 1);
                if (memberInfo.MemberId == "out") { break; }

                memberInfo.MemberPassWord = ClassInput("회원 비밀번호를 입력하세요 :  ", 1);
                if (memberInfo.MemberPassWord == "out") { break; }

                memberInfo.MemberName = ClassInput("회원명을 입력하세요 :  ", 2);
                if (memberInfo.MemberName == "out") { break; }

                memberInfo.MemberAge = ClassInput("회원나이를 입력하세요 :  ", 1);
                if (memberInfo.MemberAge == "out") { break; }

                memberInfo.MemberPhoneNumber = ClassInput("핸드폰번호를 입력하세요 :  ", 1);
                if (memberInfo.MemberPhoneNumber == "out") { break; }

                memberInfo.MemberAddress = ClassInput("회원 주소를 입력하세요 :  ", 2);
                if (memberInfo.MemberAddress == "out") { break; }

                memberInfo.MemberBorrow = 0;



                memberList.Add(memberInfo);

                regist.MemberWrite(memberList);
                Console.Clear();
                break;

            }
        }

        //회원가입 목록에 반복되는 값을 방지하기 위해 만든 함수

        public string ClassInput(string inputValue, int choice)
        {
            Console.Write(inputValue);
            string temporary = KeyEsc(choice);

            if (temporary == "out")
            {

                Console.Clear();
                return "out";
            }

            else
            {
                Console.Write("\n");

                return temporary;
            }


        }


        //창연님의 esc 적용 코드 이걸 어떻게 짰지!!!!!!
        public string KeyEsc(int select)
        {
            string ID = "";
            ConsoleKeyInfo key;
            if (select == 1)
            {


                while (true)
                {
                    key = Console.ReadKey(true);
                    if (key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Escape && IsString(key))
                    {
                        ID += key.KeyChar;
                        Console.Write(key.KeyChar);

                    }
                    else if (key.Key == ConsoleKey.Backspace && ID.Length > 0)
                    {
                        ID = ID.Substring(0, (ID.Length - 1));
                        Console.Write("\b \b");

                    }

                    else if (key.Key == ConsoleKey.Escape)
                    {

                        return "out";
                    }

                    else if (key.Key == ConsoleKey.Enter)
                    {
                        if (ID == "")
                            continue;
                        return ID;
                    }
                }
            }
            else if (select == 2)
            {
                while (true)
                {
                    key = Console.ReadKey(true);
                    if (key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Escape)
                    {
                        ID += key.KeyChar;
                        Console.Write(key.KeyChar);

                    }
                    else if (key.Key == ConsoleKey.Backspace && ID.Length > 0)
                    {
                        bool result = Regex.IsMatch(ID.Substring((ID.Length - 1), 1), @"^[a-zA-Z0-9]+$");
                        if (result)
                        {
                            ID = ID.Substring(0, (ID.Length - 1));
                            Console.Write("\b \b");
                        }
                        else
                        {
                            ID = ID.Substring(0, (ID.Length - 1));
                            Console.Write("\b \b\b");
                        }

                    }

                    else if (key.Key == ConsoleKey.Escape)
                    {

                        return "out";
                    }

                    else if (key.Key == ConsoleKey.Enter)
                    {
                        if (ID == "")
                            continue;
                        return ID;
                    }
                }
            }

            else if (select == 3)
            {
                while (true)
                {
                    key = Console.ReadKey(true);
                    if (key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Escape && IsString(key))
                    {
                        ID += key.KeyChar;
                        Console.Write("*");

                    }
                    else if (key.Key == ConsoleKey.Backspace && ID.Length > 0)
                    {
                        ID = ID.Substring(0, (ID.Length - 1));
                        Console.Write("\b \b");

                    }

                    else if (key.Key == ConsoleKey.Escape)
                    {

                        return "out";
                    }

                    else if (key.Key == ConsoleKey.Enter)
                    {
                        if (ID == "")
                            continue;
                        return ID;
                    }
                }
            }
            else
            {
                return "";
            }
        }

      
 




        bool IsString(ConsoleKeyInfo key)      //영어숫자만을 입력받기 위해인지 테스트
        {
            char trying = key.KeyChar;
            if (key == null) return false;
            if ((key.KeyChar >= 'a' && key.KeyChar <= 'z') || (key.KeyChar >= 'A' && key.KeyChar <= 'Z') || (key.KeyChar >= '0' && key.KeyChar <= '9'))
                return true;
            return false;
        }

       
    }
}
