﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManual.Model
{
    [Serializable]
    class MemberClass
    {
        private string memberId;
        private string memberPassWord;
        private string memberName;
        private string memberAge;
        private string memberPhoneNumber;
        private string memberAddress;
        private int memberBorrow;
        public List<string> bookBorrow= new List<string>();  // 빌린책을 담을 리스트
        public MemberClass() { }
        public string MemberId
        {
            get { return memberId; }
            set { memberId = value; }
        }

        public string MemberPassWord
        {
            get { return memberPassWord; }
            set { memberPassWord = value; }
        }

        public string MemberName
        {
            get { return memberName; }
            set { memberName = value; }
        }

        public string MemberAge
        {
            get { return memberAge; }
            set { memberAge = value; }
        }

        public string MemberPhoneNumber
        {
            get { return memberPhoneNumber; }
            set { memberPhoneNumber = value; }
        }

        public string MemberAddress
        {
            get { return memberAddress; }
            set { memberAddress = value; }
        }

        public int MemberBorrow
        {
            get { return memberBorrow; }
            set { memberBorrow = Convert.ToInt32(value); }
        }
    }
}
