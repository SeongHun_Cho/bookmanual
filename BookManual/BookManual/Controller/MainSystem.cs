﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManual.Controller
{
    class MainSystem
    {
        View.MenuView menu = new View.MenuView();
        ConsoleKeyInfo direction;
        bool flag = true;
        int y = 15;
        public List<Model.BookClass> bookList = new List<Model.BookClass>();    //도서 관리  리스트 생성
        public List<Model.MemberClass> memberList = new List<Model.MemberClass>();  // 멤버 관리 리스트 생성
        Controller.InfoInput info = new Controller.InfoInput();
        Controller.ManagerSystem manage = new Controller.ManagerSystem();
        Controller.LoginMenu login = new Controller.LoginMenu();
        string identy;
        string repeat="0";

        public void StartMenu()
        {

            while (true)
            {
                menu.Title();
                menu.StartMenu();
                flag = true;
                Console.SetCursorPosition(30, 15);
                while (flag)
                {
                    direction = Console.ReadKey();
                    if (direction.Key == ConsoleKey.Enter)
                    {
                        switch (y)
                        {
                            case 15:
                                Console.Clear();
                                identy = login.MemberCheck();
                                Console.Clear();
                                login.MainMenu(identy);
                                flag = false;
            
                                break;
                            case 18:

                                Console.Clear();
                                menu.Title();
                                info.MemberInput();
                                flag = false;

                                break;
                            case 21:
                                Console.Clear();
                                
                                while (repeat=="0")
                                {
                                    repeat = manage.ManageCheck();
                                }
                                if (repeat == "2") { }
                                else
                                {
                                    manage.ManageMode();
                                }
                                flag = false;

                                break;
                            case 24:
                                Environment.Exit(0);
                                break;
                        }
                    }

                    switch (direction.Key)
                    {
                        case ConsoleKey.UpArrow:
                            y = y - 3;
                            if (y < 15)
                            {
                                y = 24;
                            }
                            Console.SetCursorPosition(30, y);
                            break;
                        case ConsoleKey.DownArrow:
                            y = y + 3;
                            if (y > 26)
                            {
                                y = 15;
                            }
                            Console.SetCursorPosition(30, y);
                            break;

                    }
                }
            }
        }

    
    }
}
