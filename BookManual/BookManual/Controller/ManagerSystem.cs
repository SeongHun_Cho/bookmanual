﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManual.Controller
{
    class ManagerSystem
    {
        View.MenuView view = new View.MenuView();
        Controller.InfoInput info = new Controller.InfoInput();
        Controller.Register regist = new Controller.Register();
        List<Model.BookClass> bookList = new List<Model.BookClass>();
        List<Model.MemberClass> memberList = new List<Model.MemberClass>();
        List<string> reportList = new List<string>();
        ConsoleKeyInfo direction;
        int y = 18;
        string outcheck;
        bool flag = true;

        public string ManageCheck()
        {
            string check;
            
            view.Title();
            Console.Write("관리자 비밀번호를 입력해주세요 : ");


            check=info.KeyEsc(3);
            if (check == "byuk")
            {
                Console.Clear();
                return "1";
            }
            else if (check == "out")
            {
                Console.Clear();
                return "2";
            }
            else
            {
                
                Console.Clear();
                
                return "0";
            }
        }
        public void ManageMode()
        {
            outcheck = "";
            while (true)
            {
                view.Title();
                view.ManagerMenu();
                flag = true;
                Console.SetCursorPosition(30, 18);
                while (flag)
                {
                    direction = Console.ReadKey();
                    if (direction.Key == ConsoleKey.Enter)
                    {
                        switch (y)
                        {
                            case 18:
                                Console.Clear();
                                view.Title();
                                MemberListPrint();
                                GoBack();
                                flag = false;
                                break;
                            case 21:
                                Console.Clear();
                                view.Title();
                                BookListPrint();
                                GoBack();
                                flag = false;
                                break;
                            case 24:
                                Console.Clear();
                                view.Title();
                                MemberSearch();
                                GoBack();
                                flag = false;
                                break;
                            case 27:
                                Console.Clear();
                                view.Title();
                                MemberRemove();
                                GoBack();
                                flag = false;
                                break;
                            case 30:
                                Console.Clear();
                                view.Title();
                                BookAmountChange();
                                GoBack();
                                flag = false;
                                break;
                            case 33:
                                Console.Clear();
                                view.Title();
                                info.BookInput();
                                view.Title();
                                Console.Write("\n\n 책이 등록되었습니다. ");
                                GoBack();
                                flag = false;
                                break;
                            case 36:
                                Console.Clear();
                                view.Title();
                                BookRemove();
                                GoBack();
                                Console.Clear();
                                flag = false;
                                break;
                            case 39:
                                Console.Clear();
                                view.Title();
                                ReportPrint();
                                GoBack();
                                flag = false;
                                break;
                            case 42:
                                Console.Clear();
                                Controller.MainSystem system = new Controller.MainSystem();
                                system.StartMenu();
                                flag = false;
                                break;

                        }
                    }


                    switch (direction.Key)
                    {
                        case ConsoleKey.UpArrow:
                            y = y - 3;
                            if (y < 18)
                            {
                                y = 42;
                            }
                            Console.SetCursorPosition(30, y);
                            break;
                        case ConsoleKey.DownArrow:
                            y = y + 3;
                            if (y > 42)
                            {
                                y = 18;
                            }
                            Console.SetCursorPosition(30, y);
                            break;
                        case ConsoleKey.Escape:
                            Console.Clear();
                            outcheck = "out";
                            flag = false;
                            break;
                    }
                }
              
            }
        }

        public void BookListPrint()
        {
            bookList = regist.BookRead();
            view.BookListView();
            for (int total = 0;total< bookList.Count(); total++)
            {
                
                Console.WriteLine("                도서번호 : " + bookList[total].BookId);
                Console.WriteLine("                책 이 름 : " + bookList[total].BookName);
                Console.WriteLine("                작    가 : " + bookList[total].BookAuthor);
                Console.WriteLine("                출 판 사 : " + bookList[total].Publisher);
                Console.WriteLine("                가    격 : " + bookList[total].Price);
                Console.WriteLine("                수    량 : " + bookList[total].Amount+ "\n\n");
                Console.WriteLine("            -----------------------------------------------\n\n");
            }
        }

        public void MemberListPrint()
        {
            memberList = regist.MemberRead();
            view.MemberListVIew();

            for(int total = 0; total < memberList.Count(); total++)
            {
                Console.WriteLine("                회원이름 : " + memberList[total].MemberName);
                Console.WriteLine("                회원나이 : " + memberList[total].MemberAge);
                Console.WriteLine("                폰 번 호 : " + memberList[total].MemberPhoneNumber);
                Console.WriteLine("                주    소 : " + memberList[total].MemberAddress);
                Console.WriteLine("                대출권수 : " + memberList[total].MemberBorrow + "\n\n");
                Console.WriteLine("            -----------------------------------------------\n\n");
            }

         

        }

        public void MemberSearch()
        {
            memberList = regist.MemberRead();
            Console.Write(" 검색하려는 회원의 이름을 입력해주세요  :  ");
            string search = info.KeyEsc(2);
            if (search == "out")
            {
                GoBack();
            }
            for(int repeat = 0; repeat < memberList.Count(); repeat++)
            {
                if (memberList[repeat].MemberName.Contains(search))
                {
                    Console.WriteLine("\n\n     이름 : " + memberList[repeat].MemberName);
                    Console.WriteLine("     나이 : " + memberList[repeat].MemberAge);
                    Console.WriteLine("     핸드폰 번호 : " + memberList[repeat].MemberPhoneNumber);
                    Console.WriteLine("     주소 : " + memberList[repeat].MemberAddress);
                    Console.Write("\n\n------------------------------------------------------------------");
                }
            }
        }

        public void MemberRemove()
        {
            while (flag)
            {
                memberList = regist.MemberRead();
                Console.Write("   삭제하려는 회원의 이름을 입력해주세요 :  ");
                string search = info.KeyEsc(2);
                if (search == "out") { GoBack(); }
                for (int repeat = 0; repeat < memberList.Count(); repeat++)
                {
                    if (memberList[repeat].MemberName == search && memberList[repeat].MemberBorrow == 0)
                    {
                        memberList.RemoveAt(repeat);
                        Console.WriteLine("\n\n    삭제 성공 !!!");
                        regist.MemberWrite(memberList);
                        flag = false;
                        break;
                    }
                    else if (memberList[repeat].MemberName==search&&memberList[repeat].MemberBorrow != 0)
                    {
                        Console.WriteLine("\n\n    대출 중인 책이 있어 삭제할 수 없습니다.\n\n");
                        flag = true;
                        break;
                    }
                }
            }

        }

        public void BookAmountChange()
        {
            bookList = regist.BookRead();
            Console.Write("       수정할 책 이름을 입력하세요 : ");
            string change = info.KeyEsc(2);
            if (change == "out") { GoBack(); }
            for(int repeat = 0; repeat < bookList.Count(); repeat++)
            {
                if (bookList[repeat].BookName == change)
                {
                    Console.Write("\n  수정할 수량을 입력하세요 : ");
                    string yang = info.KeyEsc(2);
                    if (yang == "out") { GoBack(); }
                    bookList[repeat].Amount = yang;
                    regist.BookWrite(bookList);
                    break;
                }
            }
        }

        public void GoBack()
        {
            bool flag = true;
            int position = Console.CursorTop;
            Console.SetCursorPosition(0, position);
            view.BackMenu();
            position = position + 2;
            Console.SetCursorPosition(30, position);
            int standard = position;
            ConsoleKeyInfo direction;
            while (flag)
            {
                direction = Console.ReadKey();
                if (direction.Key == ConsoleKey.Enter)
                {
                    if (position == standard)
                    {
                        Console.Clear();
                        ManageMode();
                    }
                    else if (position == standard + 3)
                    {
                        Console.Clear();
                        Controller.MainSystem mainSystem = new Controller.MainSystem();
                        mainSystem.StartMenu();
                    }


                }

                else
                {
                    switch (direction.Key)
                    {
                        case ConsoleKey.UpArrow:
                            position = position - 3;
                            if (position < standard)
                            {
                                position = standard + 3;
                            }
                            Console.SetCursorPosition(30, position);
                            break;
                        case ConsoleKey.DownArrow:
                            position = position + 3;
                            if (position > standard + 3)
                            {
                                position = standard;
                            }
                            Console.SetCursorPosition(30, position);
                            break;

                    }
                }
            }
        }

        public void ReportPrint()
        {
            reportList = regist.ReportRead();
            Console.WriteLine("\n\n                           대출/반납 기록\n\n");
            for(int repeat = 0; repeat < reportList.Count(); repeat++)
            {
                Console.WriteLine("       "+reportList[repeat] + "\n\n");
            }


        }

        public void BookRemove()
        {
            bookList = regist.BookRead();
            BookListPrint();
            
            Console.Write("\n\n 삭제하고자 하는 도서 이름을 입력하세요 :");
            string bookname = info.KeyEsc(2);
            if (bookname == "out") { GoBack(); }

            for(int repeat = 0; repeat < bookList.Count(); repeat++)
            {
                if (bookList[repeat].BookName == bookname)
                {
                    bookList.RemoveAt(repeat);
                    regist.BookWrite(bookList);
                    Console.Write("\n\n 책이 삭제되었습니다. ");
                    break;
                }
            }

           
        }

    }
}
